import { $ } from 'bun'

const parseToJSON = quizString => {
  const questions = []
  let currentQuestion = null
  quizString.split('\n').forEach(line => {
    if (line.trim() === '') return // Ignore empty lines
    if (line.startsWith('###')) {
      if (currentQuestion !== null) questions.push(currentQuestion)
      currentQuestion = {
        question: line.replace(/^###\s*/, '').trim(),
        answers: [],
      }
    } else if (line.startsWith('-')) {
      const isChecked = line.includes('[x]')
      const answerText = line.replace(/^\s*-\s*\[.*?\]\s*/, '').trim()
      currentQuestion.answers.push({
        text: answerText,
        checked: isChecked,
      })
    }
  })
  if (currentQuestion !== null) {
    questions.push(currentQuestion)
  }
  return questions
}

const res = await fetch(
  'https://raw.githubusercontent.com/Ditectrev/Amazon-Web-Services-AWS-Developer-Associate-DVA-C02-Practice-Tests-Exams-Questions-Answers/main/README.md'
)
let text = await res.text()
const index = text.indexOf('###', text.indexOf('###'))
const slicedText = text.substring(index)
const parsedData = parseToJSON(slicedText)
await Bun.write(
  '/home/daniel/code/aws-developer-associate-quiz/questions.json',
  JSON.stringify(parsedData)
)

await $`git add . && git commit -m "Update questions" && git push`
